package pages;
//Purpose of this class is to return webelements

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CerotidWebsitePageElements {
	// Global Variable
	static WebElement element;

	// Course Element
	public static WebElement selectCourse(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='classType']"));
		return element;

	}

	// Session Element
	public static WebElement chooseSession(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='sessionType']"));
		return element;

	}

	// Full name Element
	public static WebElement enterName(WebDriver driver) {

		element = driver
				.findElement(By.xpath("(//input[@data-validation-required-message='Please enter your name.'])[1]"));
		return element;
	}

	// Address Element
	public static WebElement inputAddress(WebDriver driver) {

		element = driver.findElement(By.xpath("//input[@id='address']"));

		return element;
	}

}
