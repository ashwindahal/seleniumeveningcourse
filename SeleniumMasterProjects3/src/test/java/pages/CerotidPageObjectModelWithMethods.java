package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CerotidPageObjectModelWithMethods {

	// Class level variable
	WebDriver driver = null;

	// Creating By obj and finding elements by xpath
	By selectCourse = By.xpath("//select[@id='classType']");
	By selectSession = By.xpath("//select[@id='sessionType']");
	By enterName = By.xpath("//input[contains(@id,'name')]");
	By enterCity = By.xpath("//input[@id='city']");

	// Will alow me to acreate a obj of this class
	public CerotidPageObjectModelWithMethods(WebDriver driver) {
		this.driver = driver;
	}

	// Choose Course
	public void selectCourse(String courseName) {
		// creating new webelement obj
		WebElement element = driver.findElement(selectCourse);
		Select chooseCourse = new Select(element);
		chooseCourse.selectByVisibleText(courseName);

	}

	// Choose Session
	public void selectSession(String sessionName) {
		// creating new webelement obj
		WebElement element = driver.findElement(selectSession);
		Select chooseCourse = new Select(element);
		chooseCourse.selectByVisibleText(sessionName);

	}

	// Enter Name
	public void enterName(String name) {
		driver.findElement(enterName).sendKeys(name);
	}

	public void enterCityName(String city) {
		driver.findElement(enterCity);
	}

}
