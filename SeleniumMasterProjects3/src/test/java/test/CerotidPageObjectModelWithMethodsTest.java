package test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.CerotidPageObjectModelWithMethods;

public class CerotidPageObjectModelWithMethodsTest {

	// Class level variable
	public static WebDriver driver;

	public static void main(String[] args) {
		invokeBrowser();
		completeForm();

	}

	// 1: Invoke Browser
	public static void invokeBrowser() {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://www.cerotid.com");
		driver.manage().window().maximize();
	}

	// 2: Complete the Form
	public static void completeForm() {
		// Create object to access our page class
		CerotidPageObjectModelWithMethods obj = new CerotidPageObjectModelWithMethods(driver);
		obj.selectCourse("Java");
		obj.selectSession("Upcoming Session");
		obj.enterName("Kobe Bryant");
		obj.enterCityName("RIP");

	}

}
