package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import pages.CerotidWebsitePageElements;

//Created to test the page objects in CerotidWebsite
public class CerotidWebsitePageElementsTest {

	static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {

		invokeBrowserCerotidPage();
		fillForm(); 

	}

	// Step 1:
	public static void invokeBrowserCerotidPage() throws InterruptedException {
		// Set the system path to point to the chromedriver.exe file
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		// Utilizing the driver global variable and creating a new chrome driver
		driver = new ChromeDriver();

		// Navigate to Cerotid Website
		driver.navigate().to("http://www.cerotid.com");
		// Maximize the window
		driver.manage().window().maximize();

		// Waite example
		TimeUnit.SECONDS.sleep(4);

		// Refreshing the screen
		driver.navigate().refresh();

		// Adding some consition
		String titleName = "Cerotid";
		if (driver.getTitle().contains(titleName)) {
			// Prints the title
			System.out.println(driver.getTitle() + "---------------------------------- Was launched");

		} else {
			System.out.println("Expected title " + titleName + " was not seen");
			System.out.println("Test Fail----- Ending Test");
			driver.quit();
			driver.close();
		}

	}

	// Step 2:
	public static void fillForm() {
		Select chooseCourse = new Select(CerotidWebsitePageElements.selectCourse(driver));
		chooseCourse.selectByVisibleText("QA Automation");
		Select chooseSession = new Select(CerotidWebsitePageElements.chooseSession(driver));
		chooseSession.selectByVisibleText("Upcoming Session");
		//Enetering name 
		CerotidWebsitePageElements.enterName(driver).sendKeys("John Doe");
		//Entering Address
		CerotidWebsitePageElements.inputAddress(driver).sendKeys("10165 Address somewhere");	
		
		
		
		
	}

	// Step 3:

}
